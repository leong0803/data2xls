/*
// data sample
{
  expert: 'ALL-MS7-M15-280',
  symbol: 'AUDCAD',
  data: [
    { name: 'Bars', value: '99293' },
    { name: 'Ticks', value: '5905745' },
    { name: 'Total Net Profit', value: '-1514.31' },
    { name: 'Balance Drawdown Absolute', value: '2050.24' },
    { name: 'Gross Profit', value: '2186.60' },
    { name: 'Gross Loss', value: '-3700.91' },
    { name: 'Profit Factor', value: '0.59' },
    { name: 'Expected Payoff', value: '-18.93' },
    { name: 'Recovery Factor', value: '-0.65' },
    { name: 'Sharpe Ratio', value: '-0.16' },
    { name: 'LR Correlation', value: '-0.91' },
    { name: 'LR Standard Error', value: '194.17' },
    { name: 'Total Trades', value: '80' },
    { name: 'Total Deals', value: '160' },
    { name: 'Largest profit trade', value: '431.87' },
    { name: 'Average profit trade', value: '64.31' },
    { name: 'Average consecutive wins', value: '2' },
    { name: 'Symbols', value: '1' },
    { name: 'Equity Drawdown Absolute', value: '2315.50' },
    { name: 'Equity Drawdown Maximal', value: '2334.76(23.30%)' },
    { name: 'Equity Drawdown Relative', value: '23.30%(2334.76)' },
    { name: 'Margin Level', value: '54791.19%' },
    { name: 'OnTester result', value: '0' },
    { name: 'Largest loss trade', value: '-368.97' },
    { name: 'Average loss trade', value: '-80.45' },
    { name: 'Average consecutive losses', value: '2' },
    { name: 'Average position holding time', value: '311:27:11' }
  ]
}
*/

const glob = require('glob');
const fs = require('fs');

const FIELDS = [
  'Bars',
  'Ticks',
  'Total Net Profit',
  'Balance Drawdown Absolute',
  'Gross Profit',
  'Gross Loss',
  'Profit Factor',
  'Expected Payoff',
  'Recovery Factor',
  'Sharpe Ratio',
  'LR Correlation',
  'LR Standard Error',
  'Total Trades',
  'Total Deals',
  'Largest profit trade',
  'Average profit trade',
  'Average consecutive wins',
  'Symbols',
  'Equity Drawdown Absolute',
  'Equity Drawdown Maximal',
  'Equity Drawdown Relative',
  'Margin Level',
  'OnTester result',
  'Largest loss trade',
  'Average loss trade',
  'Average consecutive losses',
  'Average position holding time',
];
const FIELDS_CN = [
  '柱',
  '報價',
  '總淨盈利',
  '絕對本日餘額虧損',
  '絕對權益數虧損',
  '毛利',
  'Profit Factor',
  'Expected Payoff',
  'Recovery Factor',
  'Sharpe Ratio',
  'LR Correlation',
  'LR Standard Error',
  'Total Trades',
  'Total Deals',
  'Largest profit trade',
  'Average profit trade',
  'Average consecutive wins',
  'Symbols',
  'Equity Drawdown Absolute',
  'Equity Drawdown Maximal',
  'Equity Drawdown Relative',
  'Margin Level',
  'OnTester result',
  'Largest loss trade',
  'Average loss trade',
  'Average consecutive losses',
  'Average position holding time',
];
const RESULT_PATH = './results/';
const RESULT_FILE = () => `${RESULT_PATH}${getFullDateTime()}.csv`;

function getFullDateTime() {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0');
  const yyyy = today.getFullYear();
  return `${yyyy}-${mm}-${dd}T${today.toLocaleTimeString('en-GB').replace(/:/g, '')}+0800`;
}

function fileReader(filePath) {
  const content = fs.readFileSync(filePath, 'utf16le').toString('utf8');
  const expertRegex =
    /<td nowrap colspan="3">(?:專家|Expert):<\/td>\s+<td nowrap colspan="10" align="left"><b>([A-Za-z\-0-9]+)<\/b><\/td>/m;
  const expertMatch = content.match(expertRegex);
  if (!expertMatch) {
    console.log('Error: expert not found');
    console.log(filePath);
    return false;
  }
  const expert = expertMatch[1];
  // console.log('expert:', expert);

  const symbolRegex =
    /<td nowrap colspan="3">(?:交易品種|Symbol):<\/td>\s+<td nowrap colspan="10" align="left"><b>([A-Z]+)<\/b><\/td>/m;
  const symbolMatch = content.match(symbolRegex);
  if (!symbolMatch) {
    console.log('Error: symbol not found');
    console.log(filePath);
    return false;
  }
  const symbol = symbolMatch[1];
  // console.log('symbol:', symbol);

  // const regex1 = /<td nowrap colspan="3">([A-Za-z\s]+):<\/td>\s+<td nowrap><b>([0-9\-\s\.]+)<\/b><\/td>/gm;
  // const regex2 = /<td nowrap colspan="3">([A-Za-z\s]+):<\/td>\s+<td nowrap colspan="2"><b>(.+)<\/b><\/td>/gm;
  const regex1 = /<td nowrap colspan="3">(.+):<\/td>\s+<td nowrap><b>([0-9\-\s\.]+)<\/b><\/td>/gm;
  const regex2 = /<td nowrap colspan="3">(.+):<\/td>\s+<td nowrap colspan="2"><b>(.+)<\/b><\/td>/gm;
  // const matches = regex.exec(content);
  // const matches = content.match(regex);
  const matches = [...content.matchAll(regex1), ...content.matchAll(regex2)];

  const data = matches.map((item) => ({
    name: item[1],
    value: item[2].replace(/\s/g, ''),
  }));

  return {
    expert,
    symbol,
    data,
  };
}

const contentTemplate = (content) => {
  const headers = FIELDS.reduce((acc, field) => {
    return `${acc}"${field}",`;
  }, 'Symbol,Expert,');
  return `${headers}
${content}`;
};

const rowTemplate = (record) => {
  const { expert, symbol, data } = record;
  const content = FIELDS.reduce((acc, field, index) => {
    const value = data.find((item) => item.name === field || item.name === FIELDS_CN[index]);
    return acc + `"${value && value.value}",`;
  }, '');
  return `${symbol},${expert},${content},
`;
};

let content = '';
glob('./data/**/*.htm', null, function (er, files) {
  files.forEach((file) => {
    // console.log('file:', file);
    const result = fileReader(file);
    // console.log(result);return;
    if (!result) {
      return;
    }
    content += rowTemplate(result);
  });
  const resultFile = RESULT_FILE();
  console.log('RESULT_FILE:', resultFile);
  fs.writeFileSync(resultFile, contentTemplate(content));
  console.log('Done');
});
